<?php
/**
 *
 * Task: Bank account and payment
 *
 * @author Paweł Świszcz
 * @date 2023-02-08
 *
 */

declare(strict_types=1);

namespace App\Tests;

use App\BankAccount;
use App\BankAccountService;
use App\Transaction\OperationException;
use Money\Currency;
use Money\Money;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BankAccountTest extends KernelTestCase
{
    public function testBankAccountCurrencyIsSameWithStartingBalance(): void
    {
        $balance = new Money(0, new Currency('PLN'));

        $bankAccount = new BankAccount($balance);

        static::assertSame('PLN', $bankAccount->getBalance()->getCurrency()->getCode());
    }

    /**
     * fund empty account with 10 PLN, no any fee
     * expected result account balance 10 PLN.
     *
     * @throws OperationException
     */
    public function testFundBankAccount(): void
    {
        $balance = new Money(0, new Currency('PLN'));

        $bankAccount = new BankAccount($balance);

        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10 PLN

        static::assertSame('1000', $bankAccount->getBalance()->getAmount());
    }

    /**
     * withdraw empty account with 10 PLN
     * expected result Exception.
     */
    public function testCantWithdrawFundsWithEmptyBankAccount(): void
    {
        $balance = new Money(0, new Currency('PLN'));

        $bankAccount = new BankAccount($balance);

        $this->expectException(OperationException::class);
        $this->expectExceptionMessage('Not Enough money');
        BankAccountService::withdrawFunds($bankAccount, 1000, 'PLN'); // 10 PLN
    }

    /**
     * withdraw empty account with 10 PLN filed with 10 PLN but not enough for fee
     * expected result Exception.
     */
    public function testWithdrawFundsWithFilledBankAccountButNotEnoughWithFee(): void
    {
        $balance = new Money(1000, new Currency('PLN'));

        $bankAccount = new BankAccount($balance);

        $this->expectException(OperationException::class);
        $this->expectExceptionMessage('Not Enough money');
        BankAccountService::withdrawFunds($bankAccount, 1000, 'PLN'); // 10 PLN
    }

    /**
     * withdraw account with 10 PLN filed with 10,50 PLN but enough for fee,
     * expected result balance is 0.
     *
     * @throws OperationException
     */
    public function testWithdrawFundsWithFilledBankAccountEnoughWithFee(): void
    {
        $balance = new Money(1050, new Currency('PLN'));

        $bankAccount = new BankAccount($balance, 'PLN');

        BankAccountService::withdrawFunds($bankAccount, 1000, 'PLN'); // 10 PLN

        static::assertSame('0', $bankAccount->getBalance()->getAmount());
    }

    /**
     * expected result balance is 30 PLN.
     *
     * @throws OperationException
     */
    public function testFundAndWithdrawWithFilledBankAccount(): void
    {
        $balance = new Money(1050, new Currency('PLN'));

        $bankAccount = new BankAccount($balance);
        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10
        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10
        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10
        /*
         * balance 40,5
         */
        static::assertSame('4050', $bankAccount->getBalance()->getAmount());
        BankAccountService::withdrawFunds($bankAccount, 1000, 'PLN'); // 10 PLN

        static::assertSame('3000', $bankAccount->getBalance()->getAmount());
    }

    /**
     * daily limit for debit transaction is 3
     * expected result exception.
     *
     * @throws OperationException
     */
    public function testDailyLimitWithdrawTransactions(): void
    {
        $balance = new Money(5000, new Currency('PLN'));

        $bankAccount = new BankAccount($balance);

        BankAccountService::withdrawFunds($bankAccount, 1000, 'PLN'); // 10 PLN
        BankAccountService::withdrawFunds($bankAccount, 1000, 'PLN'); // 10 PLN
        BankAccountService::withdrawFunds($bankAccount, 1000, 'PLN'); // 10 PLN

        static::assertSame('1850', $bankAccount->getBalance()->getAmount());

        $this->expectException(OperationException::class);
        $this->expectExceptionMessage('Daily Transaction Limit extended! Current limit is 3');
        BankAccountService::withdrawFunds($bankAccount, 1000, 'PLN'); // 10 PLN

        $bankAccount->incrementDailyTransactionsCount();
        $bankAccount->incrementDailyTransactionsCount();
        $bankAccount->incrementDailyTransactionsCount();
        $bankAccount->incrementDailyTransactionsCount();

        static::assertSame('7', $bankAccount->getDailyTransactionsCount());

        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10
        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10
        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10

        static::assertSame('4050', $bankAccount->getBalance()->getAmount());
    }


    /**
     * @throws OperationException
     */
    public function testFundAfterDailyLimitWithdrawTransactionsExtended(): void
    {
        $balance = new Money(5000, new Currency('PLN'));

        $bankAccount = new BankAccount($balance);

        $bankAccount->incrementDailyTransactionsCount();
        $bankAccount->incrementDailyTransactionsCount();
        $bankAccount->incrementDailyTransactionsCount();
        $bankAccount->incrementDailyTransactionsCount();
        $bankAccount->incrementDailyTransactionsCount();
        $bankAccount->incrementDailyTransactionsCount();
        $bankAccount->incrementDailyTransactionsCount();
        $bankAccount->incrementDailyTransactionsCount();

        static::assertSame(8, $bankAccount->getDailyTransactionsCount());

        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10
        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10
        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10

        static::assertSame('8000', $bankAccount->getBalance()->getAmount());
    }

    /**
     * bank account is in EUR but fund transaction in PLN.
     * expected result exception.
     *
     * @throws OperationException
     */
    public function testCurrencyMatchAccount(): void
    {
        $balance = new Money(0, new Currency('EUR'));

        $bankAccount = new BankAccount($balance);

        $this->expectExceptionMessage('Currency does not match! Expected EUR. Provided PLN');
        $this->expectException(OperationException::class);
        BankAccountService::fundAccount($bankAccount, 1000, 'PLN'); // 10 PLN
    }
}
