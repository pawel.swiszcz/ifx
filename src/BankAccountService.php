<?php
/**
 *
 * Task: Bank account and payment
 *
 * @author Paweł Świszcz
 * @date 2023-02-08
 *
 */

declare(strict_types=1);

namespace App;

use App\Transaction\CreditTransaction;
use App\Transaction\DebitTransaction;
use App\Transaction\OperationException;
use App\Transaction\TransactionType;
use Money\Currency;
use Money\Money;

class BankAccountService
{
    /**
     * @throws OperationException
     */
    public static function fundAccount(BankAccount $bankAccount, int $amount, string $currency): void
    {
        $transaction = new CreditTransaction($bankAccount);

        self::performOperation($transaction, $amount, $currency);
    }

    /**
     * @throws OperationException
     */
    public static function withdrawFunds(BankAccount $bankAccount, int $amount, string $currency): void
    {
        $transaction = new DebitTransaction($bankAccount);

        self::performOperation($transaction, $amount, $currency);
    }

    /**
     * @throws OperationException
     */
    private static function performOperation(TransactionType $transactionType, int $amount, string $currency): void
    {
        $transactionAmountToAdd = new Money($amount, new Currency($currency));

        $transactionType->performOperation($transactionAmountToAdd);
    }
}
