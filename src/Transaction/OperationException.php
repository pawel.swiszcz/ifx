<?php
/**
 *
 * Task: Bank account and payment
 *
 * @author Paweł Świszcz
 * @date 2023-02-08
 *
 */

declare(strict_types=1);

namespace App\Transaction;

class OperationException extends \Exception
{
    public static function dailyTransactionLimit(int $limit): self
    {
        return new self("Daily Transaction Limit extended! Current limit is {$limit}");
    }

    public static function currencyDoesNotMatch(string $expected, string $provided): self
    {
        return new self("Currency does not match! Expected {$expected}. Provided {$provided}");
    }

    public static function notEnoughMoney(): self
    {
        return new self('Not Enough money');
    }
}
