<?php
/**
 *
 * Task: Bank account and payment
 *
 * @author Paweł Świszcz
 * @date 2023-02-08
 *
 */

declare(strict_types=1);

namespace App\Transaction;

use App\BankAccount;

abstract class AbstractTransaction implements TransactionType
{
    protected BankAccount $account;

    public function __construct(BankAccount $account)
    {
        $this->account = $account;
    }

    /**
     * @throws OperationException
     */
    protected function assertOperation(string $operationCurrency): void
    {
        if ($this->account->getCurrency()->getCode() !== $operationCurrency) {
            throw OperationException::currencyDoesNotMatch($this->account->getCurrency()->getCode(), $operationCurrency);
        }
    }
}
