<?php
/**
 *
 * Task: Bank account and payment
 *
 * @author Paweł Świszcz
 * @date 2023-02-08
 *
 */

declare(strict_types=1);

namespace App\Transaction;

use Money\Money;

class DebitTransaction extends AbstractTransaction
{
    private int $transactionCostPercent = 5;

    /**
     * @throws OperationException
     */
    public function performOperation(Money $money): void
    {
        $this->assertOperation($money->getCurrency()->getCode());

        $dailyLimit = 3;
        if ($dailyLimit === $this->account->getDailyTransactionsCount()) {
            throw OperationException::dailyTransactionLimit($dailyLimit);
        }

        [$transactionCost] = $money->allocate([$this->transactionCostPercent, 100 - $this->transactionCostPercent]);
        $newPotentialBalance = $this->account->getBalance()->subtract($money->add($transactionCost));

        if ($newPotentialBalance->isNegative()) {
            throw OperationException::notEnoughMoney();
        }

        $this->account->setBalance($newPotentialBalance);
        $this->account->incrementDailyTransactionsCount();
    }
}
