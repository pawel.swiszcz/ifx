<?php
/**
 *
 * Task: Bank account and payment
 *
 * @author Paweł Świszcz
 * @date 2023-02-08
 *
 */

declare(strict_types=1);

namespace App\Transaction;

use Money\Money;

class CreditTransaction extends AbstractTransaction
{
    /**
     * @throws OperationException
     */
    public function performOperation(Money $money): void
    {
        $this->assertOperation($money->getCurrency()->getCode());

        $balance = $this->account->getBalance()->add($money);

        $this->account->setBalance($balance);
    }
}
