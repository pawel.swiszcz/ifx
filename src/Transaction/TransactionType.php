<?php
/**
 *
 * Task: Bank account and payment
 *
 * @author Paweł Świszcz
 * @date 2023-02-08
 *
 */

declare(strict_types=1);

namespace App\Transaction;

use Money\Money;

interface TransactionType
{
    /**
     * @throws OperationException
     */
    public function performOperation(Money $money): void;
}
