<?php
/**
 *
 * Task: Bank account and payment
 *
 * @author Paweł Świszcz
 * @date 2023-02-08
 *
 */

declare(strict_types=1);

namespace App;

use Money\Currency;
use Money\Money;

class BankAccount
{
    private Money $balance;

    private Currency $currency;

    /*
     * debit daily transaction limit
     */
    private int $dailyTransactionsCount = 0;

    public function __construct(Money $balance)
    {
        $this->balance = $balance;
        $this->currency = $balance->getCurrency();
    }

    public function getBalance(): Money
    {
        return $this->balance;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function incrementDailyTransactionsCount(): void
    {
        ++$this->dailyTransactionsCount;
    }

    public function getDailyTransactionsCount(): int
    {
        return $this->dailyTransactionsCount;
    }

    public function setBalance(Money $balance): void
    {
        $this->balance = $balance;
    }
}
