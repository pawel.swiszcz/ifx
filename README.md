### Bank Account module

#### Installation
- docker-compose build
- docker-compose run --rm qa composer install
- docker-compose run --rm qa ./vendor/bin/phpunit --testdox

### To fix code style
- composer fix